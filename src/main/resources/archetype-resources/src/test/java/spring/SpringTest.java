package ${package}.spring;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestSpringContext.class})
public class SpringTest {

    @Autowired
    private String message;

    @Test
    public void testMessage() {
        Assert.assertEquals("Hello World !", message);
    }

}
