package ${package}.spring;

import org.springframework.context.annotation.Bean;

public class TestSpringContext {

    @Bean
    public String stringBean() {
        return "Hello World !";
    }

}
