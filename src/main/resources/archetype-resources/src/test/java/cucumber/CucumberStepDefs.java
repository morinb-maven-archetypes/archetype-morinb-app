package ${package}.cucumber;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class CucumberStepDefs {
    private String actual;
    private boolean inputBoolean;

    @Given("^a boolean true$")
    public void aBooleanTrue() {
        inputBoolean = true;
    }

    @Given("^a boolean false$")
    public void aBooleanFalse() {
        inputBoolean = false;
    }

    @When("^calling the test function$")
    public void callingTheTestFunction() {
        actual = Boolean.toString(inputBoolean);
    }

    @Then("the answer should be {string}")
    public void theAnswerShouldBe(String expected) {
        Assert.assertEquals(expected, actual);
    }
}
