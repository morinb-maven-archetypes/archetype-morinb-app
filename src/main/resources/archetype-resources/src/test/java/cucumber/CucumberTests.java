package ${package}.cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    plugin = {"pretty"}
    , features = "src/test/resources/cucumber/cucumber_scenario.feature"
    , glue = "${package}.cucumber"
    , snippets = SnippetType.CAMELCASE

)
public class CucumberTests {}
