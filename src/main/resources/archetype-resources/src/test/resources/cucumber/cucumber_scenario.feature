#noinspection SpellCheckingInspection

Feature: Base test
    Scenario: Everything is good
        Given a boolean true
        When calling the test function
        Then the answer should be "true"

    Scenario: Everything is good
        Given a boolean false
        When calling the test function
        Then the answer should be "false"
