package ${package}.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan
public class SpringContext {

    @Bean
    public String getString() {
        return "Hello from Spring bean";
    }

}
