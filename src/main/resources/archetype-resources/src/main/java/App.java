package ${package};

import ${package}.spring.SpringContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    public static void main(String[] args) {
        final ApplicationContext appContext = new AnnotationConfigApplicationContext(SpringContext.class);
        System.out.println(appContext.getBean(String.class));
    }
}
